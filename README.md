# Country CDN

This project hosts a list of countries in JSON format and provides a JavaScript file to fetch and use the data globally in your projects. The country data includes multiple fields such as ISO codes, English names, and Slovenian names.

## Table of Contents

- [Country Format](#country-format)
- [Usage](#usage)
- [License](#license)

## Country Format

Each country in the JSON file is represented with the following format:

```json
{
    "id": "705", // Represents the ISO_NUM of the country
    "iso_2": "SI", // Represents the ISO_2 of the country
    "iso_3": "SVN", // Represents the ISO_3 of the country
    "name_en": "Slovenia", // Represents the english name of the country
    "name_si": "Slovenija" // Represents the slovenian name of the country
}
```

## Usage 



To use this CDN, paste the following script into your main serving file (e.g., `index.html`):

```html
<script src="https://d3zk0.gitlab.io/countries-cdn/countries.js"></script>
```
The script copies the countries data to window.$countries.


## License
This project is licensed under the MIT License.