fetch('https://D3Zk0.gitlab.io/country-cdn/countries.json')
    .then(response => response.json())
    .then(data => {
        window.$countries = data;
        console.log('Countries data loaded:', window.$countries);

        // Dispatch a custom event to notify the application that the data is loaded
        const event = new Event('countriesLoaded');
        window.dispatchEvent(event);
    })
    .catch(error => {
        console.error('Error fetching country data:', error);
    });
